<?php

namespace App\BookTitle;

use App\Database as DB;
use App\Message\Message;

use PDO;


class BookTitle extends DB
{

    public $id = "";

    public $book_title = "";

    public $author_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title = $data['book_title'];
        }
        if(array_key_exists('author_name',$data)){
            $this->author_name = $data['author_name'];
        }
    }

    public function fetch()
    {
        $DBH = $this->conn;
        $STH = $DBH->prepare("SELECT * from book_title");
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);

        while($row = $STH->fetch()){

            echo "ID: ".$row['id'] . "<br>";

            echo "Book Title: ".$row['book_title'] . "<br>";

            echo "Author Name: ".$row['author_name'] . "<br>";
            echo "<br>";
        }
    }


    public function store(){
        $DBH = $this->conn;
        $data = array($this->book_title,$this->author_name);
        $STH = $DBH->prepare("INSERT INTO `book_title`(`id`, `book_title`, `author_name`) VALUES (NULL ,?,?)");
        $STH->execute($data);

        
    }





}