
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book Title</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Create Book Title</h2>
<form class="form-horizontal" method="post" action="store.php">
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Book Title:</label>
        <div class="col-sm-4">
            <input type="text" name="book_title" class="form-control" id="book_title" placeholder="Enter Book Title" size="10px">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Auhtor Name:</label>
        <div class="col-sm-4">
            <input type="text" name="author_name" class="form-control" id="author_name" placeholder="Enter Author Name" >
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-info">Create</button>
        </div>
    </div>
</form>
</body>
</html>




